import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Accounts extends BaseSchema {
    protected tableName = 'accounts';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('accounts_id_seq')");
            table.string('login', 20).notNullable();
            table.string('password', 60).notNullable();
            table.string('email', 255).unique().notNullable();
            table.string('first_name', 255).nullable();
            table.string('last_name', 255).nullable();
            table.string('remember_token');
            table.timestamp('created_at');
            table.timestamp('updated_at');
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// CREATE TABLE IF NOT EXISTS accounts
// (
//     id integer NOT NULL DEFAULT nextval('accounts_id_seq'::regclass),
//     login character varying(20) NOT NULL,
//     password character varying(60) NOT NULL,
//     email character varying(255)  NOT NULL,
//     first_name character varying(255),
//     last_name character varying(255),
//     remember_token character varying(100),
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT accounts_pkey PRIMARY KEY (id),
//     CONSTRAINT accounts_email_unique UNIQUE (email)
// )
