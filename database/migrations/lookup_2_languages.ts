import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Languages extends BaseSchema {
    protected tableName = 'languages';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('languages_id_seq')");
            table.string('part2_b', 3).notNullable();
            table.string('part2_t', 3).notNullable();
            table.string('part1', 2).notNullable();
            table.string('scope', 20).notNullable();
            table.string('type', 20).notNullable();
            table.string('ref_name', 150).notNullable();
            table.boolean('active').notNullable().defaultTo(true);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: languages
// CREATE TABLE IF NOT EXISTS languages
// (
//     id integer NOT NULL DEFAULT nextval('languages_id_seq'::regclass),
//     part2_b character varying(3) NOT NULL,
//     part2_t character varying(3) NOT NULL,
//     part1 character varying(2) NOT NULL,
//     scope character varying(20) NOT NULL,
//     type character varying(20) NOT NULL,
//     ref_name character varying(150) NOT NULL,
//     active boolean NOT NULL DEFAULT true,
//     CONSTRAINT languages_pkey PRIMARY KEY (id)
// )
