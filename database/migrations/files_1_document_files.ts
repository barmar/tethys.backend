import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class DocumentFiles extends BaseSchema {
    protected tableName = 'document_files';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('document_files_id_seq')");
            table.integer('document_id').unsigned().notNullable();
            table
                .foreign('document_id', 'document_files_document_id_foreign')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // delete this file when document is deleted
                .onUpdate('CASCADE');
            table.string('path_name').notNullable();
            table.string('label');
            table.string('comment');
            table.string('mime_type');
            table.string('language');
            table.bigInteger('file_size').notNullable();
            table.boolean('visible_in_frontdoor').notNullable().defaultTo(true);
            table.boolean('visible_in_oai').notNullable().defaultTo(true);
            table.integer('sort_order').notNullable();
            table.timestamp('created_at');
            table.timestamp('updated_at');
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: document_files
// CREATE TABLE IF NOT EXISTS document_files
// (
//     id integer NOT NULL DEFAULT nextval('document_files_id_seq'::regclass),
//     document_id integer NOT NULL,
//     path_name character varying(100) NOT NULL,
//     label character varying(100),
//     comment character varying(255),
//     mime_type character varying(255),
//     language character varying(3),
//     file_size bigint NOT NULL,
//     visible_in_frontdoor boolean NOT NULL DEFAULT true,
//     visible_in_oai boolean NOT NULL DEFAULT true,
//     sort_order integer NOT NULL,
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT document_files_pkey PRIMARY KEY (id),
//     CONSTRAINT document_files_document_id_foreign FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// )
