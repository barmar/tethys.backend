import BaseSchema from '@ioc:Adonis/Lucid/Schema';
import { DatasetTypes, ServerStates } from 'Contracts/enums';

export default class Documents extends BaseSchema {
    protected tableName = 'documents';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('documents_id_seq')");
            table.string('contributing_corporation');
            table.string('creating_corporation').notNullable();
            table.string('publisher_name');
            table.timestamp('embargo_date');
            table.integer('publish_id').unique();
            table.integer('project_id').unsigned().nullable();
            table
                .foreign('project_id', 'documents_project_id_foreign')
                .references('id')
                .inTable('projects')
                .onDelete('NO ACTION') // don't delete this doc when project is deleted
                .onUpdate('NO ACTION');
            table.enum('type', Object.keys(DatasetTypes)).notNullable();
            table.string('language').notNullable();
            table.enum('server_state', Object.values(ServerStates)).notNullable().defaultTo("'inprogress'");
            table.boolean('belongs_to_bibliography').notNullable().defaultTo(false);
            table.timestamp('created_at').notNullable();
            table.timestamp('server_date_modified').notNullable();
            table.timestamp('server_date_published');
            table.integer('account_id').unsigned().nullable();
            table
                .foreign('account_id', 'documents_account_id_foreign')
                .references('id')
                .inTable('accounts')
                .onDelete('NO ACTION') // don't delete this doc when account is deleted
                .onUpdate('NO ACTION');
            table.integer('editor_id');
            table.integer('reviewer_id');
            table.string('preferred_reviewer');
            table.string('preferred_reviewer_email');
            table.string('reject_editor_note');
            table.string('reject_reviewer_note');
            table.boolean('reviewer_note_visible').notNullable().defaultTo(false);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: documents
// CREATE TABLE IF NOT EXISTS documents
// (
//     id integer NOT NULL DEFAULT nextval('documents_id_seq'::regclass),
//     contributing_corporation character varying(255) ,
//     creating_corporation character varying(255)  NOT NULL,
//     publisher_name character varying(255) ,
//     embargo_date timestamp(0) without time zone,
//     publish_id integer,
//     project_id integer,
//     type character varying(255)  NOT NULL,
//     language character varying(10)  NOT NULL,
//     server_state character varying(255)  NOT NULL DEFAULT 'inprogress'::character varying,
//     belongs_to_bibliography boolean NOT NULL DEFAULT false,
//     created_at timestamp(0) without time zone NOT NULL,
//     server_date_modified timestamp(0) without time zone NOT NULL,
//     server_date_published timestamp(0) without time zone,
//     account_id integer,
//     editor_id integer,
//     reviewer_id integer,
//     preferred_reviewer character varying(25) ,
//     preferred_reviewer_email character varying(50) ,
//     reject_editor_note character varying(500) ,
//     reject_reviewer_note character varying(500) ,
//     reviewer_note_visible boolean NOT NULL DEFAULT false,
//     CONSTRAINT documents_pkey PRIMARY KEY (id),
//     CONSTRAINT documents_publish_id_unique UNIQUE (publish_id),
//     CONSTRAINT documents_project_id_foreign FOREIGN KEY (project_id)
//         REFERENCES projects (id) MATCH SIMPLE
//         ON UPDATE NO ACTION
//         ON DELETE NO ACTION,
// 	CONSTRAINT documents_account_id_foreign FOREIGN KEY (account_id)
//         REFERENCES accounts (id) MATCH SIMPLE
//         ON UPDATE NO ACTION
//         ON DELETE NO ACTION,
//     CONSTRAINT documents_server_state_check CHECK (server_state::text = ANY (ARRAY['deleted'::character varying::text, 'inprogress'::character varying::text, 'published'::character varying::text, 'released'::character varying::text, 'editor_accepted'::character varying::text, 'approved'::character varying::text, 'rejected_reviewer'::character varying::text, 'rejected_editor'::character varying::text, 'reviewed'::character varying::text])),
//     CONSTRAINT documents_type_check CHECK (type::text = ANY (ARRAY['analysisdata'::character varying::text, 'measurementdata'::character varying::text, 'monitoring'::character varying::text, 'remotesensing'::character varying::text, 'gis'::character varying::text, 'models'::character varying::text, 'mixedtype'::character varying::text]))
// )
