import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class DocumentLicences extends BaseSchema {
    protected tableName = 'document_licences';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('document_licences_id_seq')");
            table.boolean('active').notNullable().defaultTo(true);
            table.string('comment_internal', 4000);
            table.string('desc_markup', 4000);
            table.string('desc_text', 4000);
            table.string('language', 3);
            table.string('link_licence', 200).notNullable();
            table.string('link_logo', 200);
            table.string('link_sign', 200);
            table.string('mime_type', 30);
            table.string('name_long', 200).notNullable();
            table.string('name', 50).notNullable();
            table.boolean('pod_allowed').notNullable().defaultTo(false);
            table.specificType('sort_order', 'smallint').notNullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: document_licences
// CREATE TABLE IF NOT EXISTS document_licences
// (
//     id integer NOT NULL DEFAULT nextval('document_licences_id_seq'::regclass),
//     active boolean NOT NULL DEFAULT true,
//     comment_internal character varying(4000),
//     desc_markup character varying(4000),
//     desc_text character varying(4000),
//     language character varying(3),
//     link_licence character varying(200) NOT NULL,
//     link_logo character varying(200),
//     link_sign character varying(200),
//     mime_type character varying(30),
//     name_long character varying(200) NOT NULL,
//     name character varying(50) NOT NULL,
//     pod_allowed boolean NOT NULL DEFAULT false,
//     sort_order smallint NOT NULL,
//     CONSTRAINT document_licences_pkey PRIMARY KEY (id)
// )
