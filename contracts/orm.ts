declare module '@ioc:Adonis/Lucid/Orm' {
    interface ModelQueryBuilderContract<Model extends LucidModel, Result = InstanceType<Model>> {
        pluck(value: string, id?: string): Promise<Object>;
    }
}
