declare module '@ioc:Adonis/Core/Validator' {
    interface Rules {
        translatedLanguage(mainLanguageField: string, typeField: string): Rule;
        uniqueArray(field: string): Rule;
    }
}
