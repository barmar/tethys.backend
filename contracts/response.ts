declare module '@ioc:Adonis/Core/Response' {
    interface ResponseContract {
        flash(key: string, message: any): this;

        toRoute(route: string): this;
    }
}
