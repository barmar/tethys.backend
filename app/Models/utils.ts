import Database, {
    // DatabaseQueryBuilderContract,
    QueryClientContract,
    TransactionClientContract,
} from '@ioc:Adonis/Lucid/Database';
import Config from '@ioc:Adonis/Core/Config';

export function getUserRoles(userId: number, trx?: TransactionClientContract): Promise<Array<string>> {
    const { userRole } = Config.get('acl.joinTables');
    return ((trx || Database) as QueryClientContract | TransactionClientContract)
        .query()
        .from('roles')
        .distinct('roles.slug')
        .leftJoin(userRole, `${userRole}.role_id`, 'roles.id')
        .where(`${userRole}.user_id`, userId)
        .then((res) => {
            return res.map((r) => r.slug);
        });
}
