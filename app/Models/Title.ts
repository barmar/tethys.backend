import { column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm';
import Dataset from './Dataset';
import BaseModel from './BaseModel';
// import { DatasetRelatedBaseModel } from './BaseModel';

export default class Title extends BaseModel {
    public static primaryKey = 'id';
    public static table = 'dataset_titles';
    public static selfAssignPrimaryKey = false;
    public static timestamps = false;
    public static fillable: string[] = ['value', 'type', 'language'];

    @column({})
    public document_id: number;

    @column()
    public type: string;

    @column()
    public value: string;

    @column()
    public language: string;

    @belongsTo(() => Dataset, {
        foreignKey: 'document_id',
    })
    public dataset: BelongsTo<typeof Dataset>;
}
