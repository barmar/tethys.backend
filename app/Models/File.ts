import { DateTime } from 'luxon';
import {
    column,
    hasMany,
    HasMany,
    belongsTo,
    BelongsTo,
    // manyToMany,
    // ManyToMany,
    SnakeCaseNamingStrategy,
} from '@ioc:Adonis/Lucid/Orm';
import HashValue from './HashValue';
import Dataset from './Dataset';
import BaseModel from './BaseModel';

export default class File extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'document_files';
    public static selfAssignPrimaryKey = false;

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public document_id: number;

    @column({})
    public pathName: string;

    @column()
    public label: string;

    @column()
    public comment: string;

    @column()
    public mimeType: string;

    @column()
    public language: string;

    @column()
    public fileSize: number;

    @column()
    public visibleInOai: boolean;

    @column()
    public visibleInFrontdoor: boolean;

    @column()
    public sortOrder: number;

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime;

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime;

    // public function dataset()
    // {
    //     return $this->belongsTo(Dataset::class, 'document_id', 'id');
    // }
    @belongsTo(() => Dataset, {
        foreignKey: 'document_id',
    })
    public dataset: BelongsTo<typeof Dataset>;

    @hasMany(() => HashValue, {
        foreignKey: 'file_id',
    })
    public hashvalues: HasMany<typeof HashValue>;
}
