import {
    column,
    SnakeCaseNamingStrategy,
    manyToMany,
    ManyToMany,
    belongsTo,
    BelongsTo,
    hasMany,
    HasMany,
    computed,
    hasOne,
    HasOne,
} from '@ioc:Adonis/Lucid/Orm';
import { DateTime } from 'luxon';
import dayjs from 'dayjs';
import Person from './Person';
import User from './User';
import Title from './Title';
import Description from './Description';
import License from './License';
import Subject from './Subject';
import File from './File';
import Coverage from './Coverage';
import DatasetReference from './DatasetReference';
import Collection from './Collection';
import DatasetIdentifier from './DatasetIdentifier';
import Project from './Project';
import DocumentXmlCache from './DocumentXmlCache';
import DatasetExtension from 'App/Models/Traits/DatasetExtension'; // Adjust the import path

export default class Dataset extends DatasetExtension {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'documents';
    public static selfAssignPrimaryKey = false;

    @column({ isPrimary: true })
    public id: number;

    @column({})
    public server_state: string;

    @column({})
    public publisher_name: string;

    @column({ columnName: 'creating_corporation' })
    public creating_corporation: string;

    @column.dateTime({ columnName: 'embargo_date' })
    public embargo_date: DateTime;

    @column({})
    public type: string;

    @column({})
    public language: string;

    @column({})
    public publish_id: number | null = null;

    @column({})
    public project_id: number | null = null;

    @column({})
    public account_id: number | null = null;

    @column({})
    public editor_id: number | null = null;

    @column({})
    public reviewer_id: number | null = null;

    @column({})
    public reject_editor_note: string | null;

    @column({})
    public preferred_reviewer: string | null;

    @column({})
    public preferred_reviewer_email: string | null;

    @column({})
    public reject_reviewer_note: string | null;

    @column.dateTime({ columnName: 'server_date_published' })
    public server_date_published: DateTime;

    // @column.dateTime({ autoCreate: true, columnName: 'created_at' })
    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        columnName: 'created_at',
    })
    public created_at: DateTime;

    @column.dateTime({ autoCreate: true, autoUpdate: true, columnName: 'server_date_modified' })
    public server_date_modified: DateTime;

    @manyToMany(() => Person, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'person_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact'],
    })
    public persons: ManyToMany<typeof Person>;

    /**
     * Get the account that the dataset belongs to
     */
    @belongsTo(() => User, {
        foreignKey: 'account_id',
    })
    public user: BelongsTo<typeof User>;

    @belongsTo(() => Project, {
        foreignKey: 'project_id',
    })
    public project: BelongsTo<typeof Project>;

    @hasMany(() => Title, {
        foreignKey: 'document_id',
    })
    public titles: HasMany<typeof Title>;

    @hasMany(() => Description, {
        foreignKey: 'document_id',
    })
    public descriptions: HasMany<typeof Description>;

    @manyToMany(() => License, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'licence_id',
        pivotTable: 'link_documents_licences',
    })
    public licenses: ManyToMany<typeof License>;

    @manyToMany(() => Subject, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'subject_id',
        pivotTable: 'link_dataset_subjects',
    })
    public subjects: ManyToMany<typeof Subject>;

    @hasMany(() => File, {
        foreignKey: 'document_id',
    })
    public files: HasMany<typeof File>;

    @hasOne(() => Coverage, {
        foreignKey: 'dataset_id',
    })
    public coverage: HasOne<typeof Coverage>;

    @hasMany(() => DatasetReference, {
        foreignKey: 'document_id',
    })
    public references: HasMany<typeof DatasetReference>;

    // Dataset.hasMany(Reference, {
    //     foreignKey: "related_document_id",
    //     as: "referenced_by",
    // });
    @hasMany(() => DatasetReference, {
        foreignKey: 'related_document_id',
    })
    public referenced_by: HasMany<typeof DatasetReference>;

    @manyToMany(() => Collection, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'collection_id',
        pivotTable: 'link_documents_collections',
    })
    public collections: ManyToMany<typeof Collection>;

    @hasOne(() => DatasetIdentifier, {
        foreignKey: 'dataset_id',
    })
    public identifier: HasOne<typeof DatasetIdentifier>;

    @computed({
        serializeAs: 'main_title',
    })
    public get mainTitle() {
        // return `${this.firstName} ${this.lastName}`;
        const mainTitle = this.titles?.find((title) => title.type === 'Main');
        return mainTitle ? mainTitle.value : null;
    }

    @manyToMany(() => Person, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'person_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact'],
        onQuery(query) {
            query.wherePivot('role', 'author');
        },
    })
    public authors: ManyToMany<typeof Person>;

    @manyToMany(() => Person, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'person_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact'],
        onQuery(query) {
            query.wherePivot('role', 'contributor');
        },
    })
    public contributors: ManyToMany<typeof Person>;

    @hasOne(() => DocumentXmlCache, {
        foreignKey: 'document_id',
    })
    public xmlCache: HasOne<typeof DocumentXmlCache>;
}
