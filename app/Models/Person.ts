import { column, SnakeCaseNamingStrategy, computed, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm';
import { DateTime } from 'luxon';
import dayjs from 'dayjs';
import Dataset from './Dataset';
import BaseModel from './BaseModel';

export default class Person extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'persons';
    public static selfAssignPrimaryKey = false;
    // only the academic_title, email, first_name, identifier_orcid, last_name  and name_type attributes are allowed to be mass assigned.
    public static fillable: string[] = ['academic_title', 'email', 'first_name', 'identifier_orcid', 'last_name', 'name_type'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({ columnName: 'academic_title' })
    public academicTitle: string;

    @column()
    public email: string;

    @column({})
    public firstName: string;

    @column({})
    public lastName: string;

    @column({})
    public identifierOrcid: string;

    @column({})
    public status: boolean;

    @column({})
    public nameType: string;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
    })
    public createdAt: DateTime;

    @computed({
        serializeAs: 'name',
    })
    public get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    // @computed()
    // public get progress(): number {
    // 	return 50;
    // }

    // @computed()
    // public get created_at() {
    // 	return '2023-03-21 08:45:00';
    // }

    @computed()
    public get datasetCount() {
        const stock = this.$extras.datasets_count; //my pivot column name was "stock"
        return stock;
    }

    @manyToMany(() => Dataset, {
        pivotForeignKey: 'person_id',
        pivotRelatedForeignKey: 'document_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact'],
    })
    public datasets: ManyToMany<typeof Dataset>;
}
