import { column, BaseModel, belongsTo, BelongsTo, SnakeCaseNamingStrategy } from '@ioc:Adonis/Lucid/Orm';
import File from './File';

export default class HashValue extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'file_id, type';
    public static table = 'file_hashvalues';

    // static get primaryKey () {
    //     return 'type, value'
    //   }

    public static get incrementing() {
        return false;
    }

    // @column({
    // 	isPrimary: true,
    // })
    // public id: number;

    // Foreign key is still on the same model
    @column({})
    public file_id: number;

    @column({})
    public type: string;

    @column()
    public value: string;

    @belongsTo(() => File)
    public file: BelongsTo<typeof File>;
}
