import { column, SnakeCaseNamingStrategy, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm';
import { DateTime } from 'luxon';
import Dataset from './Dataset';
import BaseModel from './BaseModel';

export default class DatasetReference extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'document_references';
    public static fillable: string[] = ['value', 'label', 'type', 'relation'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public document_id: number;

    @column({})
    public related_document_id?: number;

    @column({})
    public type: string;

    @column({})
    public relation: string;

    @column({})
    public value: string;

    @column({})
    public label: string;

    @column.dateTime({
        autoCreate: true,
    })
    public created_at?: DateTime;

    @column.dateTime({
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at?: DateTime;

    @belongsTo(() => Dataset, {
        foreignKey: 'document_id',
    })
    public dataset: BelongsTo<typeof Dataset>;

    // Reference.belongsTo(Dataset, {
    //     foreignKey: "related_document_id",
    //     as: "new_dataset",
    //     include: "identifier"
    // });
    @belongsTo(() => Dataset, {
        foreignKey: 'related_document_id',
    })
    public new_dataset: BelongsTo<typeof Dataset>;
}
