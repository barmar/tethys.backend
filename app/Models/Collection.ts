import { column, SnakeCaseNamingStrategy, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm';
import Dataset from './Dataset';
import BaseModel from './BaseModel';

export default class Collection extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'collections';
    public static fillable: string[] = ['name', 'number', 'role_id'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public document_id: number;

    @column({})
    public role_id?: number;

    @column({})
    public number?: string;

    @column({})
    public name: string;

    @column({})
    public oai_subset?: string;

    @column({})
    public parent_id?: number;

    @column({})
    public visible: boolean;

    @column({})
    public visible_publish: boolean;

    @manyToMany(() => Dataset, {
        pivotForeignKey: 'collection_id',
        pivotRelatedForeignKey: 'document_id',
        pivotTable: 'link_documents_collections',
    })
    public datasets: ManyToMany<typeof Dataset>;
}
