import { column, SnakeCaseNamingStrategy, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm';
import { DateTime } from 'luxon';
import Dataset from './Dataset';
import BaseModel from './BaseModel';

export default class DatasetIdentifier extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'dataset_identifiers';
    public static fillable: string[] = ['value', 'label', 'type', 'relation'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public dataset_id: number;

    @column({})
    public type: string;

    @column({})
    public value: string;

    @column.dateTime({
        autoCreate: true,
    })
    public created_at?: DateTime;

    @column.dateTime({
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at?: DateTime;

    @belongsTo(() => Dataset, {
        foreignKey: 'dataset_id',
    })
    public dataset: BelongsTo<typeof Dataset>;
}
