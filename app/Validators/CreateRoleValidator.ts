import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator';
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';

export default class CreateRoleValidator {
    constructor(protected ctx: HttpContextContract) {}

    /*
     * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
     *
     * For example:
     * 1. The username must be of data type string. But then also, it should
     *    not contain special characters or numbers.
     *    ```
     *     schema.string({}, [ rules.alpha() ])
     *    ```
     *
     * 2. The email must be of data type string, formatted as a valid
     *    email. But also, not used by any other user.
     *    ```
     *     schema.string({}, [
     *       rules.email(),
     *       rules.unique({ table: 'users', column: 'email' }),
     *     ])
     *    ```
     */
    public schema = schema.create({
        name: schema.string({ trim: true }, [
            rules.minLength(3),
            rules.maxLength(255),
            rules.unique({ table: 'roles', column: 'name' }),
            rules.regex(/^[a-zA-Z0-9-_]+$/), //Must be alphanumeric with hyphens or underscores
        ]),
        display_name: schema.string.optional({ trim: true }, [
            rules.minLength(3),
            rules.maxLength(255),
            rules.unique({ table: 'roles', column: 'name' }),
            rules.regex(/^[a-zA-Z0-9-_]+$/), //Must be alphanumeric with hyphens or underscores
        ]),
        description: schema.string.optional({}, [rules.minLength(3), rules.maxLength(255)]),
        permissions: schema.array([rules.minLength(1)]).members(schema.number()), // define at least one role for the new role
    });

    // emails: schema
    //   .array([rules.minLength(1)])
    //   .members(
    //     schema.object().members({ email: schema.string({}, [rules.email()]) })
    //   ),

    /**
     * Custom messages for validation failures. You can make use of dot notation `(.)`
     * for targeting nested fields and array expressions `(*)` for targeting all
     * children of an array. For example:
     *
     * {
     *   'profile.username.required': 'Username is required',
     *   'scores.*.number': 'Define scores as valid numbers'
     * }
     *
     */
    public messages: CustomMessages = {
        'minLength': '{{ field }} must be at least {{ options.minLength }} characters long',
        'maxLength': '{{ field }} must be less then {{ options.maxLength }} characters long',
        'required': '{{ field }} is required',
        'unique': '{{ field }} must be unique, and this value is already taken',
        'confirmed': '{{ field }} is not correct',
        'permissions.minLength': 'at least {{ options.minLength }} permission must be defined',
        'permissions.*.number': 'Define roles as valid numbers',
    };
}
