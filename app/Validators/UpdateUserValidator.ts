import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator';
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
// import { Request } from '@adonisjs/core/build/standalone';

export default class UpdateUserValidator {
    protected ctx: HttpContextContract;
    public schema;

    constructor(ctx: HttpContextContract) {
        this.ctx = ctx;
        this.schema = this.createSchema();
    }

    // public get schema() {
    //     return this._schema;
    // }

    private createSchema() {
        return schema.create({
            login: schema.string({ trim: true }, [
                rules.minLength(3),
                rules.maxLength(50),
                rules.unique({
                    table: 'accounts',
                    column: 'login',
                    // whereNot: { id: this.refs.id }
                    whereNot: { id: this.ctx?.params.id },
                }),
                // rules.regex(/^[a-zA-Z0-9-_]+$/),
                //Must be alphanumeric with hyphens or underscores
            ]),
            email: schema.string({}, [
                rules.email(),
                rules.unique({ table: 'accounts', column: 'email', whereNot: { id: this.ctx?.params.id } }),
            ]),
            password: schema.string.optional([rules.confirmed(), rules.minLength(6)]),
            roles: schema.array.optional([rules.minLength(1)]).members(schema.number()), // define at least one role for the new user
        });
    }

    /*
     * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
     *
     * For example:
     * 1. The username must be of data type string. But then also, it should
     *    not contain special characters or numbers.
     *    ```
     *     schema.string({}, [ rules.alpha() ])
     *    ```
     *
     * 2. The email must be of data type string, formatted as a valid
     *    email. But also, not used by any other user.
     *    ```
     *     schema.string({}, [
     *       rules.email(),
     *       rules.unique({ table: 'users', column: 'email' }),
     *     ])
     *    ```
     */

    // public refs = schema.refs({
    // 	id: this.ctx.params.id
    // })

    // public schema = schema.create({
    // 	login: schema.string({ trim: true }, [
    // 		rules.minLength(3),
    // 		rules.maxLength(50),
    // 		rules.unique({
    //     table: 'accounts',
    //     column: 'login',
    // 	// whereNot: { id: this.refs.id }
    //     whereNot: { id: this.ctx?.params.id },
    //   }),
    // 		// rules.regex(/^[a-zA-Z0-9-_]+$/),
    //   //Must be alphanumeric with hyphens or underscores
    // 	]),
    // 	email: schema.string({}, [rules.email(), rules.unique({ table: 'accounts', column: 'email' })]),
    // 	password: schema.string.optional([rules.confirmed(), rules.minLength(6)]),
    // 	roles: schema.array([rules.minLength(1)]).members(schema.number()), // define at least one role for the new user
    // });

    /**
     * Custom messages for validation failures. You can make use of dot notation `(.)`
     * for targeting nested fields and array expressions `(*)` for targeting all
     * children of an array. For example:
     *
     * {
     *   'profile.username.required': 'Username is required',
     *   'scores.*.number': 'Define scores as valid numbers'
     * }
     *
     */
    public messages: CustomMessages = {
        'minLength': '{{ field }} must be at least {{ options.minLength }} characters long',
        'maxLength': '{{ field }} must be less then {{ options.maxLength }} characters long',
        'required': '{{ field }} is required',
        'unique': '{{ field }} must be unique, and this value is already taken',
        'confirmed': '{{ field }} is not correct',
        'roles.minLength': 'at least {{ options.minLength }} role must be defined',
        'roles.*.number': 'Define roles as valid numbers',
    };
}
