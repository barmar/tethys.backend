export default class Field {
    // private _multiplicity: number | string = 1;
    private _hasMultipleValues: boolean = false;
    private _valueModelClass: string | null = null;
    private _linkModelClass: string | null = null;
    // private _owningModelClass: string | null = null;
    private _value: any;
    private _name: string;

    constructor(name: string) {
        this._name = name;
        this._value = null;
    }

    getValueModelClass(): any {
        return this._valueModelClass;
    }

    setValueModelClass(classname: any): Field {
        this._valueModelClass = classname;
        return this;
    }

    getName(): string {
        return this._name;
    }

    // setOwningModelClass(classname: string): Field {
    //     this._owningModelClass = classname;
    //     return this;
    // }

    public setValue(value: string | string[] | number | boolean): Field {
        if (value === null || value === this._value) {
            return this;
        }

        if (Array.isArray(value) && value.length === 0) {
            value = [];
        } else if (typeof value === 'boolean') {
            value = value ? 1 : 0;
        } else {
            this._value = value;
        }
        return this;
    }
    public getValue() {
        return this._value;
    }

    hasMultipleValues(): boolean {
        return this._hasMultipleValues;
    }

    setMultiplicity(max: number | '*'): Field {
        if (max !== '*') {
            if (typeof max !== 'number' || max < 1) {
                throw new Error('Only integer values > 1 or "*" allowed.');
            }
        }
        // this._multiplicity = max;
        this._hasMultipleValues = (typeof max == 'number' && max > 1) || max === '*';
        return this;
    }

    getLinkModelClass(): string | null {
        return this._linkModelClass;
    }
}
