/*
|--------------------------------------------------------------------------
| Http Exception Handler
|--------------------------------------------------------------------------
|
| AdonisJs will forward all exceptions occurred during an HTTP request to
| the following class. You can learn more about exception handling by
| reading docs.
|
| The exception handler extends a base `HttpExceptionHandler` which is not
| mandatory, however it can do lot of heavy lifting to handle the errors
| properly.
|
*/

import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import Logger from '@ioc:Adonis/Core/Logger';
import HttpExceptionHandler from '@ioc:Adonis/Core/HttpExceptionHandler';

export default class ExceptionHandler extends HttpExceptionHandler {
    protected statusPages = {
        '401,403': 'errors/unauthorized',
        '404': 'errors/not-found',
        '500..599': 'errors/server-error',
    };

    constructor() {
        super(Logger);
    }

    public async handle(error: any, ctx: HttpContextContract) {
        const { response, request, inertia } = ctx;

        /**
         * Handle failed authentication attempt
         */
        // if (['E_INVALID_AUTH_PASSWORD', 'E_INVALID_AUTH_UID'].includes(error.code)) {
        //   session.flash('errors', { login: error.message });
        //   return response.redirect('/login');
        // }
        // if ([401].includes(error.status)) {
        //   session.flash('errors', { login: error.message });
        //   return response.redirect('/dashboard');
        // }

        // https://github.com/inertiajs/inertia-laravel/issues/56
        if (request.header('X-Inertia') && [500, 503, 404, 403, 401].includes(response.getStatus())) {
            return inertia.render('Error', {
                status: response.getStatus(),
                message: error.message,
            });
            // ->toResponse($request)
            // ->setStatusCode($response->status());
        }

        /**
         * Forward rest of the exceptions to the parent class
         */
        return super.handle(error, ctx);
    }
}
