import { StatusCodes } from 'http-status-codes';
import HTTPException from './HttpException';

class InternalServerErrorException extends HTTPException {
    constructor(message?: string) {
        super(StatusCodes.INTERNAL_SERVER_ERROR, message || 'Server Error');
        this.stack = '';
    }
}
export default InternalServerErrorException;
