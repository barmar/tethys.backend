import { Response } from '@adonisjs/http-server/build/src/Response';
import { ServerResponse, IncomingMessage } from 'http';
import { RouterContract } from '@ioc:Adonis/Core/Route';
import { EncryptionContract } from '@ioc:Adonis/Core/Encryption';
import { ResponseConfig, ResponseContract } from '@ioc:Adonis/Core/Response';

class FlashResponse extends Response implements ResponseContract {
    protected static macros = {};
    protected static getters = {};
    constructor(
        public request: IncomingMessage,
        public response: ServerResponse,
        flashEncryption: EncryptionContract,
        flashConfig: ResponseConfig,
        flashRouter: RouterContract,
    ) {
        super(request, response, flashEncryption, flashConfig, flashRouter);
    }
    public nonce: string;

    public flash(key: string, message: any): this {
        // Store the flash message in the session
        this.ctx?.session.flash(key, message);
        return this;
    }

    public toRoute(route: string): this {
        // Redirect to the specified route
        super.redirect().toRoute(route);
        return this;
    }
}

export default FlashResponse;
