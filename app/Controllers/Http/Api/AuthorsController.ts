import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import Person from 'App/Models/Person';
// import Dataset from 'App/Models/Dataset';

// node ace make:controller Author
export default class AuthorsController {
    public async index({}: HttpContextContract) {
        // select * from gba.persons
        // where exists (select * from gba.documents inner join gba.link_documents_persons on "documents"."id" = "link_documents_persons"."document_id"
        // 			  where ("link_documents_persons"."role" = 'author') and ("persons"."id" = "link_documents_persons"."person_id"));
        const authors = await Person.query()
            .whereHas('datasets', (dQuery) => {
                dQuery.wherePivot('role', 'author');
            })
            .withCount('datasets', (query) => {
                query.as('datasets_count');
            });

        return authors;
    }

    public async persons({ request }: HttpContextContract) {
        const authors = Person.query().where('status', true);

        if (request.input('filter')) {
            // users = users.whereRaw('name like %?%', [request.input('search')])
            const searchTerm = request.input('filter');
            authors.whereILike('first_name', `%${searchTerm}%`).orWhereILike('last_name', `%${searchTerm}%`);
            // .orWhere('email', 'like', `%${searchTerm}%`);
        }

        let persons = await authors;
        return persons;
    }
}
