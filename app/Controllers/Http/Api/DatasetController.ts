import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
// import Person from 'App/Models/Person';
import Dataset from 'App/Models/Dataset';
import { StatusCodes } from 'http-status-codes';

// node ace make:controller Author
export default class DatasetController {
    public async index({}: HttpContextContract) {
        // select * from gba.persons
        // where exists (select * from gba.documents inner join gba.link_documents_persons on "documents"."id" = "link_documents_persons"."document_id"
        // 			  where ("link_documents_persons"."role" = 'author') and ("persons"."id" = "link_documents_persons"."person_id"));
        const datasets = await Dataset.query().where('server_state', 'published').orWhere('server_state', 'deleted');

        return datasets;
    }

    public async findAll({ response }: HttpContextContract) {
        try {
            const datasets = await Dataset.query()
                .where('server_state', 'published')
                .orWhere('server_state', 'deleted')
                .preload('descriptions') // Preload any relationships you need
                .orderBy('server_date_published');
            return response.status(StatusCodes.OK).json(datasets);
        } catch (error) {
            return response.status(500).json({
                message: error.message || 'Some error occurred while retrieving datasets.',
            });
        }
    }

    public async findOne({ params }: HttpContextContract) {
        const datasets = await Dataset.query()
            .where('publish_id', params.publish_id)
            .preload('titles')
            .preload('descriptions')
            .preload('user')
            .preload('authors', (builder) => {
                builder.orderBy('pivot_sort_order', 'asc');
            })
            .preload('contributors', (builder) => {
                builder.orderBy('pivot_sort_order', 'asc');
            })
            .preload('subjects')
            .preload('coverage')
            .preload('licenses')
            .preload('references')
            .preload('project')
            .preload('referenced_by', (builder) => {
                builder.preload('dataset', (builder) => {
                    builder.preload('identifier');
                });
            })
            .preload('files', (builder) => {
                builder.preload('hashvalues');
            })
            .preload('identifier')
            .firstOrFail();

        return datasets;
    }
}
