import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import Database from '@ioc:Adonis/Lucid/Database';
import { StatusCodes } from 'http-status-codes';

export default class HomeController {
    public async findDocumentsPerYear({ response, params }: HttpContextContract) {
        const year = params.year;
        const from = parseInt(year);
        const serverState = 'published';
        try {
            // Database.raw(`date_part('year', server_date_published) as pub_year`)
            // const datasets = await Dataset.query()
            //     .select(['id', 'publish_id', 'server_date_published', ])
            //     .where('server_state', serverState)
            //     .andWhereRaw(`date_part('year', server_date_published) = ?`, [from])
            //     .preload('titles')
            //     .preload('authors')
            //     .orderBy('server_date_published');

            const datasets = await Database.from('documents as doc')
                .select(['publish_id', 'server_date_published', Database.raw(`date_part('year', server_date_published) as pub_year`)])
                .where('server_state', serverState)
                .innerJoin('link_documents_persons as ba', 'doc.id', 'ba.document_id')
                .andWhereRaw(`date_part('year', server_date_published) = ?`, [from])
                .orderBy('server_date_published');

            return response.json(datasets);
        } catch (error) {
            return response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
                message: error.message || 'Some error occurred while retrieving datasets.',
            });
        }
    }

    public async findYears({ response }: HttpContextContract) {
        const serverState = 'published';
        // Use raw SQL queries to select all cars which belongs to the user
        try {
            const datasets = await Database.rawQuery(
                'SELECT distinct EXTRACT(YEAR FROM server_date_published) as published_date FROM gba.documents WHERE server_state = ?',
                [serverState],
            );

            // Pluck the ids of the cars
            const years = datasets.rows.map((dataset) => dataset.published_date);
            // check if the cars is returned
            // if (years.length > 0) {
            return response.status(StatusCodes.OK).json(years);
            // }
        } catch (error) {
            return response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
                message: 'An error occurred while retrieving the list of publication years from the Tethys repository.',
            });
        }
    }
}
