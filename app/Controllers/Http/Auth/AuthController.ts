import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
// import User from 'App/Models/User';
// import Hash from '@ioc:Adonis/Core/Hash';
// import InvalidCredentialException from 'App/Exceptions/InvalidCredentialException';
import AuthValidator from 'App/Validators/AuthValidator';

export default class AuthController {
    //   login function
    public async login({ request, response, auth, session }: HttpContextContract) {
        // console.log({
        // 	registerBody: request.body(),
        // });
        await request.validate(AuthValidator);

        const plainPassword = await request.input('password');
        const email = await request.input('email');
        // grab uid and password values off request body
        // const { email, password } = request.only(['email', 'password'])

        try {
            // attempt to login
            await auth.use('web').attempt(email, plainPassword);
        } catch (error) {
            // if login fails, return vague form message and redirect back
            session.flash('message', 'Your username, email, or password is incorrect');
            return response.redirect().back();
        }

        // otherwise, redirect todashboard
        response.redirect('/dashboard');
    }

    //   logout function
    public async logout({ auth, response }: HttpContextContract) {
        // await auth.logout();
        await auth.use('web').logout();
        response.redirect('/app/login');
        // return response.status(200);
    }
}
