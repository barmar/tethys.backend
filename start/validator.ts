/*
|--------------------------------------------------------------------------
| Preloaded File
|--------------------------------------------------------------------------
|
| Any code written inside this file will be executed during the application
| boot.
https://issuehunt.io/r/adonisjs/validator/issues/84
|
*/
// import { string } from '@ioc:Adonis/Core/Helpers';
import { validator } from '@ioc:Adonis/Core/Validator';

validator.rule('uniqueArray', (dataArray, [field], { pointer, arrayExpressionPointer, errorReporter }) => {
    const array = dataArray; //validator.helpers.getFieldValue(data, field, tip);

    if (!Array.isArray(array)) {
        throw new Error(`The ${pointer} must be an array.`);
    }

    const uniqueValues = new Set();
    for (let i = 0; i < array.length; i++) {
        const item = array[i];
        const attributeValue = item[field]; // Extract the attribute value for uniqueness check

        if (uniqueValues.has(attributeValue)) {
            // throw new Error(`The ${field} array contains duplicate values for the ${field} attribute.`)
            errorReporter.report(
                pointer,
                'uniqueArray', // Keep an eye on this
                `The ${pointer} array contains duplicate values for the ${field} attribute.`,
                arrayExpressionPointer,
                { field, array: pointer },
            );
            return;
        }

        uniqueValues.add(attributeValue);
    }
});

validator.rule(
    'translatedLanguage',
    (value, [mainLanguageField, typeField], { root, tip, pointer, arrayExpressionPointer, errorReporter }) => {
        if (typeof value !== 'string') {
            return;
        }
        // const fieldValue = validator. getValue(data, field)
        // this should return the "category_id" value present in "root", but i got undefined
        const mainLanguage = validator.helpers.getFieldValue(mainLanguageField, root, tip);
        const type = validator.helpers.getFieldValue(typeField, root, tip);

        if (type && type === 'Translated') {
            if (value === mainLanguage) {
                errorReporter.report(
                    pointer,
                    'translatedLanguage', // Keep an eye on this
                    'translatedLanguage validation failed',
                    arrayExpressionPointer,
                    { mainLanguage },
                );
            }
        }

        // if (value !== string.camelCase(value)) {
        //     options.errorReporter.report(
        //         options.pointer,
        //         'camelCase',
        //         'camelCase validation failed',
        //         options.arrayExpressionPointer
        //     );
        // }
    },
);

validator.rule('fileExtension', async (value, [extensions], { pointer, arrayExpressionPointer, errorReporter }) => {
    const allowedExtensions = extensions.map((ext: string) => ext.toLowerCase());
    const uploadedFile = value;

    if (!uploadedFile) {
        return;
    }

    const extension = uploadedFile.extname.toLowerCase().replace('.', '');

    if (!allowedExtensions.includes(extension)) {
        errorReporter.report(
            pointer,
            'fileExtension',
            'Invalid file extension. Only {{ extensions }} files are allowed.',
            arrayExpressionPointer,
        );
    }
});

// validator.rule(
//     'clamavScan',
//     (value, [field], { root, tip, pointer, arrayExpressionPointer, errorReporter }) => {
//         if (typeof value !== 'object') {
//             return;
//         }
//         const uploadedFile = validator.helpers.getFieldValue(field, root, tip);
//     // return rules.file({}, [
//     //     async (file) => {
//     //         const clamdhost = process.env['CLAMD_HOST'] ?? '127.0.0.1';
//     //         const clamdport = Number(process.env['CLAMD_PORT']) ?? '3310';
//     //         try {
//     //             var isInfected = await scanFileForViruses(file.tmpPath, clamdhost, clamdport);
//     //         } catch (error) {
//     //             throw new Error(`${pointer}: ${error.message}`);
//     //         }
//     //     },
//     // ]);
// });

// async function scanFileForViruses(filePath, host, port): Promise<boolean> {
//     // const clamscan = await (new ClamScan().init());
//     const opts: ClamScan.Options = {
//         preference: 'clamdscan',
//         clamdscan: {
//             active: true,
//             host,
//             port,
//             multiscan: true,
//         },
//     };
//     const clamscan = await new ClamScan().init(opts);

//     return new Promise((resolve, reject) => {
//         clamscan.isInfected(filePath, (err, file, isInfected: boolean) => {
//             if (err) {
//                 reject(err);
//             } else if (isInfected) {
//                 reject(new Error(`File ${file} is infected!`));
//             } else {
//                 resolve(isInfected);
//             }
//         });
//     });
// }
