import Route from '@ioc:Adonis/Core/Route';

// API
Route.group(() => {
    // Route.post("register", "AuthController.register");
    // Route.post("login", "AuthController.login");

    Route.group(() => {
        Route.get('authors', 'AuthorsController.index').as('author.index');
        Route.get('datasets', 'DatasetController.index').as('dataset.index');
        Route.get('persons', 'AuthorsController.persons').as('author.persons');
        // Route.get("author/:id", "TodosController.show");
        // Route.put("author/update", "TodosController.update");
        // Route.post("author", "TodosController.store");
        Route.get('/dataset', 'DatasetController.findAll').as('dataset.findAll');
        Route.get('/dataset/:publish_id', 'DatasetController.findOne').as('dataset.findOne');
        Route.get('/sitelinks/:year', 'HomeController.findDocumentsPerYear');
        Route.get('/years', 'HomeController.findYears');
    });
    // .middleware("auth:api");
})
    .namespace('App/Controllers/Http/Api')
    .prefix('api');
