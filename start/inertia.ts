/*
|--------------------------------------------------------------------------
| Inertia Preloaded File
|--------------------------------------------------------------------------
|
| Any code written inside this file will be executed during the application
| boot.
|
*/

import Inertia from '@ioc:EidelLev/Inertia';
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';

Inertia.share({
    errors: (ctx) => {
        return ctx.session.flashMessages.get('errors');
    },

    flash: (ctx) => {
        return {
            message: ctx.session.flashMessages.get('message'),
            warning: ctx.session.flashMessages.get('warning'),
        };
    },

    // params: ({ params }) => params,
    authUser: ({ auth }: HttpContextContract) => {
        if (auth.user) {
            return auth.user;
            // {
            //     'id': auth.user.id,
            //     'login': auth.user.login,
            // };
        } else {
            return null;
        }
    },
}).version(() => Inertia.manifestFile('public/assets/manifest.json'));

// 'flash' => [
//   'message' => fn () => $request->session()->get('message'),
// ],
