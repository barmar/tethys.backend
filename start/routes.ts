/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route';
// import Inertia from '@ioc:EidelLev/Inertia';
import AuthValidator from 'App/Validators/AuthValidator';
import HealthCheck from '@ioc:Adonis/Core/HealthCheck';
import User from 'App/Models/User';
// import AuthController from 'App/Controllers/Http/Auth/AuthController';
import './routes/api';

Route.get('health', async ({ response }) => {
    const report = await HealthCheck.getReport();
    return report.healthy ? response.ok(report) : response.badRequest(report);
});

Route.get('/', async ({ view }) => {
    return view.render('welcome');
}).as('dashboard2');

// Route.get( '/oai', 'Oai/RequestController.index').as('oai');
Route.group(() => {
    Route.get('/oai', 'Oai/OaiController.index').as('get');
    Route.post('/oai', 'Oai/OaiController.index').as('post');
}).as('oai');
// Route.inertia('/about', 'App');

Route.group(() => {
    Route.get('/', async ({ inertia }) => {
        const users = await User.query().orderBy('login');
        return inertia.render('App', {
            testing: 'this is a test',
            users: users,
        });
    }).as('index');

    // Route.get('/login', async ({ inertia }) => {
    // 	return inertia.render('Auth/Login');
    // }).as('login.show');

    Route.get('/register', async ({ inertia }) => {
        return inertia.render('register-view/register-view-component');
    }).as('register.show');

    Route.post('/register', async ({ request, response }) => {
        console.log({
            registerBody: request.body(),
        });

        const data = await request.validate(AuthValidator);
        console.log({ data });

        return response.redirect().toRoute('app.index');
    }).as('register.store');
})
    .prefix('app')
    .as('app');

Route.get('/dashboard', async ({ inertia }) => {
    return inertia.render('Dashboard');
})
    .as('dashboard')
    .middleware('auth');

// Route.on("/login").render("signin");
Route.get('/app/login', async ({ inertia }) => {
    return inertia.render('Auth/Login');
}).as('app.login.show');

// Route.post("/login", "Users/AuthController.login");
Route.post('/app/login', 'Auth/AuthController.login').as('login.store');
// Route.on("/signup").render("signup");
// Route.post("/signup", "AuthController.signup");
Route.post('/signout', 'Auth/AuthController.logout').as('logout');

Route.group(() => {
    Route.get('/settings', async ({ inertia }) => {
        return inertia.render('Admin/Settings');
    }).as('settings');

    Route.get('/user', 'UsersController.index').as('user.index').middleware(['can:user-list']);
    Route.get('/user/create', 'UsersController.create').as('user.create').middleware(['can:user-create']);
    Route.post('/user/store', 'UsersController.store').as('user.store').middleware(['can:user-create']);
    Route.get('/user/:id', 'UsersController.show').as('user.show').where('id', Route.matchers.number());
    Route.get('/user/:id/edit', 'UsersController.edit').as('user.edit').where('id', Route.matchers.number()).middleware(['can:user-edit']);
    Route.put('/user/:id/update', 'UsersController.update')
        .as('user.update')
        .where('id', Route.matchers.number())
        .middleware(['can:user-edit']);
    Route.delete('/user/:id', 'UsersController.destroy')
        .as('user.destroy')
        .where('id', Route.matchers.number())
        .middleware(['can:user-delete']);
    // Route.resource('user', 'UsersController');

    Route.get('/role', 'RoleController.index').as('role.index').middleware(['can:user-list']);
    Route.get('/role/create', 'RoleController.create').as('role.create').middleware(['can:user-create']);
    Route.post('/role/store', 'RoleController.store').as('role.store').middleware(['can:user-create']);
    Route.get('/role/:id', 'RoleController.show').as('role.show').where('id', Route.matchers.number());
    Route.get('/role/:id/edit', 'RoleController.edit').as('role.edit').where('id', Route.matchers.number()).middleware(['can:user-edit']);
    Route.put('/role/:id/update', 'RoleController.update')
        .as('role.update')
        .where('id', Route.matchers.number())
        .middleware(['can:user-edit']);
    Route.delete('/role/:id', 'RoleController.destroy')
        .as('role.destroy')
        .where('id', Route.matchers.number())
        .middleware(['can:user-delete']);
})
    .namespace('App/Controllers/Http/Admin')
    .prefix('admin')
    // .middleware(['auth', 'can:dataset-list,dataset-publish']);
    .middleware(['auth', 'is:administrator,moderator']);

Route.get('/edit-account-info', 'UsersController.accountInfo')
    .as('admin.account.info')
    .namespace('App/Controllers/Http/Admin')
    .middleware(['auth']);

Route.post('/edit-account-info/store/:id', 'UsersController.accountInfoStore')
    .as('admin.account.info.store')
    .where('id', Route.matchers.number())
    .namespace('App/Controllers/Http/Admin')
    .middleware(['auth']);
// Route::post('change-password', 'UserController@changePasswordStore')->name('admin.account.password.store');

Route.group(() => {
    // Route.get('/user', 'UsersController.index').as('user.index');
    Route.get('/dataset', 'DatasetController.index').as('dataset.list').middleware(['auth']); //.middleware(['can:dataset-list']);
    Route.get('/dataset/create', 'DatasetController.create').as('dataset.create').middleware(['auth', 'can:dataset-submit']);
    Route.post('/dataset/first/first-step', 'DatasetController.firstStep')
        .as('dataset.first.step')
        .middleware(['auth', 'can:dataset-submit']);
    Route.post('/dataset/second/second-step', 'DatasetController.secondStep')
        .as('dataset.second.step')
        .middleware(['auth', 'can:dataset-submit']);
    Route.post('/dataset/second/third-step', 'DatasetController.thirdStep')
        .as('dataset.third.step')
        .middleware(['auth', 'can:dataset-submit']);

    Route.post('/dataset/submit', 'DatasetController.store').as('dataset.submit').middleware(['auth', 'can:dataset-submit']);

    Route.get('/dataset/:id/release', 'DatasetController.release')
        .as('dataset.release')
        .where('id', Route.matchers.number())
        .middleware(['auth']); //, 'can:dataset-submit']);
    Route.put('/dataset/:id/releaseupdate', 'DatasetController.releaseUpdate')
        .as('dataset.releaseUpdate')
        .middleware(['auth', 'can:dataset-submit']);
    Route.get('/dataset/:id/edit', 'DatasetController.edit')
        .as('dataset.edit')
        .where('id', Route.matchers.number())
        .middleware(['auth', 'can:dataset-submit']);
    Route.get('/dataset/:id/delete', 'DatasetController.delete').as('dataset.delete').middleware(['auth', 'can:dataset-delete']);
    Route.put('/dataset/:id/deleteupdate', 'DatasetController.deleteUpdate')
        .as('dataset.deleteUpdate')
        .middleware(['auth', 'can:dataset-delete']);

    Route.get('/person', 'PersonController.index').as('person.index').middleware(['auth']);
    // Route.get('/user/:id/edit', 'UsersController.edit').as('user.edit').where('id', Route.matchers.number());
    // Route.put('/user/:id/update', 'UsersController.update').as('user.update').where('id', Route.matchers.number());
    // Route.delete('/user/:id', 'UsersController.destroy').as('user.destroy').where('id', Route.matchers.number());
    // Route.resource('user', 'DatasetController');
})
    .namespace('App/Controllers/Http/Submitter')
    .prefix('submitter');
// .middleware(['auth', 'can:dataset-list,dataset-publish']);
// .middleware(['auth', 'is:submitter']);
