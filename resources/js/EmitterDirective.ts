import type { Directive, Plugin } from 'vue';
import type { EventEmitter } from '@/Components/Map/EventEmitter';

// https://vuejs.org/guide/reusability/custom-directives.html#directive-hooks
// https://learnvue.co/articles/vue-custom-directives
let emitter: EventEmitter<string>;

export const EmitterDirective: Directive<any> = {
    mounted: (el, binding, vnode) => {
        // el: the element the directive is bound to. This can be used to directly manipulate
        binding.arg && el.setAttribute('event', binding.arg);

        // const vm = binding.instance;
        // let iwas = vm?.$data.onMapInitializedEvent;
        // var handlers = (vnode.data && vnode.data.on) ||
        // (vnode.componentOptions && vnode.componentOptions.listeners);

        //vnode: the underlying VNode representing the bound element.

        //binding.instance: The instance of the component where the directive is used.
        // const instance = binding.instance;

        // if (vnode && vnode.dirs) {
        //     const instance = vnode.dirs[0].instance;
        // }

        // binding.value
        // arg: 'onMapInitialized'
        // dir:  {created: ƒ}
        // instance: Proxy {__v_skip: true}
        // modifiers: {}
        // oldValue: undefined
        // value:  {mapId: 'test', name: 'test'}
        const handler = binding.value;

        // let instanceComp = binding.instance;
        // @ts-ignore
        if (vnode.ctx) {
            // @ts-ignore
            emitter = vnode.ctx.data[binding.arg];
            emitter.on(handler);
        }

        // for (const key in binding.value) {
        //     const k = key; //kebabCase(key);
        //     if (!el.hasAttribute(k)) {
        //         el.setAttribute(k, binding.value[key]);
        //     }
        // }
    },
    unmounted(binding) {
        const handler = binding.value;
        emitter.off(handler);
    },
    // getSSRProps(binding) {
    //     /* c8 ignore next */
    //     if (!binding.value) return {};

    //     return Object.fromEntries(Object.entries(binding.value).map(([key, value]) => [kebabCase(key), value]));
    // },
};

export const EmitterPlugin: Plugin = {
    install(app) {
        app.directive('bind-event', EmitterDirective);
    },
};
