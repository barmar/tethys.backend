// import * as util from '../core/utilities';
import { EventEmitter } from './EventEmitter';
import type { Map } from 'leaflet/src/map/index';

export abstract class Control<T> extends EventEmitter<T> {
    // @section
    // @aka Control options
    public options = {
        position: 'topright',
    };
    protected _map;
    protected _container;

    // constructor(defaults?) {
    //     super();
    //     if (!(this instanceof Control)) {
    //         throw new TypeError("Control constructor cannot be called as a function.");
    //     }
    //     // properties
    //     // util.setOptions(this, defaults);
    // }

    public getPosition() {
        return this.options.position;
    }

    public getContainer() {
        return this._container;
    }

    public abstract onRemove(map): void;

    public abstract onAdd(map: any): HTMLElement;

    public addTo(map: Map): Control<T> {
        this._map = map;

        let container = (this._container = this.onAdd(map));
        let pos = this.getPosition(); //"topright"
        let corner = map.controlCorners[pos];
        if (container) {
            // $(container).addClass('gba-control');
            container.classList.add('gba-control');

            if (pos.indexOf('bottom') !== -1) {
                corner.insertBefore(container, corner.firstChild);
            } else {
                corner.appendChild(container);
            }
        }
        return this;
    }

    public removeFrom(map) {
        let pos = this.getPosition();
        let corner = map._controlCorners[pos];

        corner.removeChild(this._container);
        this._map = null;

        if (this.onRemove) {
            this.onRemove(map);
        }
        return this;
    }
}
