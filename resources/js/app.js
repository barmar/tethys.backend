import '../css/app.css';
import { createApp, h } from 'vue';
import { Inertia } from '@inertiajs/inertia';

import { createInertiaApp, Link, usePage } from '@inertiajs/vue3';
// import DefaultLayout from '@/Layouts/Default.vue';

import { createPinia } from 'pinia';
import { StyleService } from '@/Stores/style';
import { LayoutService } from '@/Stores/layout';
import { MainService } from '@/Stores/main';
import { darkModeKey, styleKey } from '@/config';
const pinia = createPinia();
import { EmitterPlugin } from '@/EmitterDirective';

import { initRoutes } from '@eidellev/adonis-stardust/client';
initRoutes();

// import '@fontsource/archivo-black/index.css';
// import '@fontsource/inter/index.css';

createInertiaApp({
    progress: {
        // color: '#4B5563',
        color: '#22C55E',
    },
    //   resolve: (name) => {
    //     const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
    //     return pages[`./Pages/${name}.vue`]
    //   },
    // Webpack
    // resolve: (name) => require(`./Pages/${name}`),
    // resolve: (name) => require(`./Pages/${name}.vue`),
    // add default layout
    resolve: (name) => {
        const page = require(`./Pages/${name}.vue`).default;
        // if (!page.layout) {
        // 	page.layout = DefaultLayout;
        // }
        return page;
    },
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(pinia)
            .use(EmitterPlugin)
            // .component('inertia-link', Link)
            .mount(el);
    },
});

const styleService = StyleService(pinia);
const layoutService = LayoutService(pinia);
// const mainService = MainService(pinia);
// mainService.setUser(user);

/* App style */
styleService.setStyle(localStorage[styleKey] ?? 'basic');

/* Dark mode */
if ((!localStorage[darkModeKey] && window.matchMedia('(prefers-color-scheme: dark)').matches) || localStorage[darkModeKey] === '1') {
    styleService.setDarkMode(true);
}

/* Collapse mobile aside menu on route change */
Inertia.on('navigate', (event) => {
    layoutService.isAsideMobileExpanded = false;
    layoutService.isAsideLgActive = false;
});
