// import Swal, { SweetAlertOptions } from 'sweetalert2';

// Where “target” represents the class containing the method we’re decorating, “propertyKey” represents the method name,
// and “descriptor” will contain the method implementation. Within the decorator function we are able to do things before /
// after the method executes, or skip execution altogether based on arbitrary conditions. For example

// export function Test(target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
export function Test(descriptor: PropertyDescriptor) {
    // we first cache the original method implementation
    const originalMethod = descriptor.value;

    // then we overwrite it with a new implementation,
    // ...args represent the original arguments

    descriptor.value = function (...args) {
        // do custom logic before executing the original method
        // execute original method with the original arguments
        const result = originalMethod.apply(this, args);
        // execute custom logic after executing the original method
        return result;
    };

    return descriptor;
}
// export function Confirmable(target?: Object, propertyKey: string, descriptor: PropertyDescriptor) {
// export function Confirmable(descriptor: PropertyDescriptor) {
//     // Again, cache the original method for later use
//     const originalMethod = descriptor.value;

//     // the configuration object for sweetalert
//     // const config: SweetAlertOptions = {
//     const config: SweetAlertOptions = {
//         title: 'Are you sure?',
//         html: 'Do you want to perform this action?',
//         showDenyButton: true,
//         confirmButtonText: 'Yes',
//         denyButtonText: 'No',
//         icon: 'question',
//     };

//     // we write a new implementation for the method
//     descriptor.value = async function (...args) {
//         const res = await Swal.fire(config);
//         // fire sweetalert with the config object
//         if (res.isConfirmed) {
//             // if user clicked yes,
//             // we run the original method with the original arguments
//             const result = originalMethod.apply(this, args);

//             // and return the result
//             return result;
//         }
//     };

//     return descriptor;
// }
