import { mdiMonitor, mdiGithub, mdiAccountEye, mdiAccountGroup, mdiDatabasePlus } from '@mdi/js';

export default [
    {
        route: 'dashboard',
        icon: mdiMonitor,
        label: 'Dashboard',
    },
    // {
    //   route: 'permission.index',
    //   icon: mdiAccountKey,
    //   label: 'Permissions'
    // },
    // {
    //   route: 'role.index',
    //   icon: mdiAccountEye,
    //   label: 'Roles'
    // },
    {
        route: 'user.index',
        icon: mdiAccountGroup,
        label: 'Users',
    },
    {
        route: 'role.index',
        icon: mdiAccountEye,
        label: 'Roles',
    },
    {
        href: '/oai',
        icon: mdiAccountEye,
        label: 'OAI',
        target: '_blank',
    },
    {
        // route: 'dataset.create',
        icon: mdiDatabasePlus,
        label: 'Submitter',
        children: [
            {
                route: 'dataset.list',
                icon: mdiDatabasePlus,
                label: 'All my datasets',
            },
            {
                route: 'dataset.create',
                icon: mdiDatabasePlus,
                label: 'Create Dataset',
            },
        ],
    },
    // {
    //     route: 'dataset.create',
    //     icon: mdiDatabasePlus,
    //     label: 'Create Dataset',
    // },
    {
        href: 'https://gitea.geologie.ac.at/geolba/tethys',
        icon: mdiGithub,
        label: 'Gitea',
        target: '_blank',
    },
];
