// notiwind.ts
import { createNotifier, NotificationGroup, defineNotificationComponent } from 'notiwind';

export type NotificationSchema = {
    type: string;
    title?: string;
    text: string;
};

export const notify = createNotifier<NotificationSchema>();
export const Notification = defineNotificationComponent<NotificationSchema>();
export { NotificationGroup };
