import { Ref } from 'vue';

export interface Dataset {
    [key: string]:
        | string
        | Ref<string>
        | boolean
        | Array<Title>
        | Array<Description>
        | Array<Person>
        | number
        | (IErrorMessage | undefined)
        | Coverage
        | Array<DatasetReference>
        | Array<File>;
    language: Ref<string>;
    // licenses: Array<number>;
    rights: boolean;
    type: string;
    creating_corporation: string;
    titles: Array<Title>;
    descriptions: Array<Description>;
    authors: Array<Person>;
    contributors: Array<Person>;
    project_id?: number;
    embargo_date?: string;
    coverage: Coverage;
    errors?: IErrorMessage;
    // async (user): Promise<void>;
    subjects: Array<Subject>;
    references: Array<DatasetReference>;
    files: Array<TestFile> | undefined;
    // upload: TethysFile
}

/** Provides information about files and allows JavaScript in a web page to access their content. */
export interface TestFile extends Blob {
    readonly lastModified: number;
    readonly name: string;
    readonly webkitRelativePath: string;
    label: string;
    sorting: number;
}

// export interface TethysFile {
//     label: string,
//     sorting: number,
//     upload: File,
// }

export interface Subject {
    // id: number;
    language: string;
    type: string;
    value: string;
    external_key?: string;
}
export interface DatasetReference {
    // id: number;
    value: string;
    label: string;
    type: string;
    relation: string;
}

export interface Title {
    value: string;
    type: string;
    language: string | Ref<string>;
}

export interface Description {
    value: string;
    type: string;
    language: string | Ref<string>;
}

export interface Person {
    id: number;
    name: string;
    email: string;
    name_type: string;
    identifier_orcid: string;
    datasetCount: string;
    created_at: string;
}

interface IErrorMessage {
    [key: string]: Array<string>;
}

export interface Coverage {
    x_min?: number;
    y_min?: number;
    x_max?: number;
    y_max?: number;
    elevation_min?: number;
    elevation_max?: number;
    elevation_absolut?: number;

    depth_min?: number;
    depth_max?: number;
    depth_absolut?: number;

    time_min?: number;
    time_max?: number;
    time_absolut?: number;
}
