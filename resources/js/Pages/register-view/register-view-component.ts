import { Component, Vue, Prop } from 'vue-facing-decorator';
import AuthLayout from '@/Layouts/Auth.vue';
// import { Inertia } from '@inertiajs/inertia';
import { NButton } from 'naive-ui';
import { useForm, InertiaForm, router } from '@inertiajs/vue3';
import FormInput from '@/Components/FormInput.vue'; //          @/Components/FormInput.vue'
// import { defineComponent, reactive } from 'vue';

// export default defineComponent({
// 	layout: AuthLayout,

// 	components: {
// 		NButton,
// 		FormInput,
// 	},

// 	setup() {
// 		// const form = useForm({
// 		// 	email: '',
// 		// 	password: ''
// 		// });
// 		const form = reactive({
// 			email: null,
// 			password: null,
// 		});

// 		const submit = async () => {
// 			await Inertia.post('/app/register', form);
// 		};

// 		return { form, submit };
// 	},
// });

export interface IErrorMessage {
    [key: string]: Array<string>;
}

@Component({
    options: {
        layout: AuthLayout,
    },
    name: 'RegisterViewComponent',
    components: {
        NButton,
        FormInput,
    },
})
export default class RegisterViewComponent extends Vue {
    // Component Property
    @Prop({
        type: Object,
        default: () => ({}),
    })
    public errors: IErrorMessage;

    // Data Property
    public form: InertiaForm<any> = useForm({
        email: '',
        password: '',
    });

    public results: Array<any> = [];

    // Component method
    public async submit(): Promise<void> {
        // await Inertia.post('/app/register', this.form);
        await router.post('/app/register', this.form);
    }
}
