import { defineStore } from 'pinia';
import axios from 'axios';
import { Dataset } from '@/Dataset';

export interface Person {
    id: number;
    name: string;
    email: string;
    name_type: string;
    identifier_orcid: string;
    datasetCount: string;
    created_at: string;
}

interface TransactionItem {
    amount: number;
    account: string;
    name: string;
    date: string;
    type: string;
    business: string;
}

export const MainService = defineStore('main', {
    state: () => ({
        /* User */
        userName: '',
        userEmail: null,
        userAvatar: null,

        /* Field focus with ctrl+k (to register only once) */
        isFieldFocusRegistered: false,

        /* Sample data for starting dashboard(commonly used) */
        clients: [],
        history: [] as Array<TransactionItem>,

        // api based data
        authors: [] as Array<Person>,
        // persons: [] as Array<Person>,
        datasets: [],

        dataset: {} as Dataset,
    }),
    actions: {
        // payload = authenticated user
        setUser(payload) {
            if (payload.name) {
                this.userName = payload.name;
            }
            if (payload.email) {
                this.userEmail = payload.email;
            }
            if (payload.avatar) {
                this.userAvatar = payload.avatar;
            }
        },

        setDataset(payload) {
            this.dataset = payload;

            // this.dataset = {
            //     language: language,
            //     licenses: payload.licenses,
            //     rights: payload.rights,
            //     type: payload.type,
            //     creating_corporation: payload.creating_corporation,
            //     titles: payload.titles,
            //     descriptions: payload.descriptions,
            //     authors: payload.authors,
            //     project_id: payload.project_id,
            //     embargo_date: payload.embargo_date,
            // } as Dataset;
            // for (let index in payload.titles) {
            //     let title = payload.titles[index];
            //     title.language = language;
            // }
        },

        clearDataset() {
            this.dataset = null;
        },

        fetch(sampleDataKey) {
            // sampleDataKey= clients or history
            axios
                .get(`data-sources/${sampleDataKey}.json`)
                .then((r) => {
                    if (r.data && r.data.data) {
                        this[sampleDataKey] = r.data.data;
                    }
                })
                .catch((error) => {
                    alert(error.message);
                });
        },

        fetchApi(sampleDataKey) {
            // sampleDataKey= authors or datasets
            axios
                .get(`api/${sampleDataKey}`)
                .then((r) => {
                    if (r.data) {
                        this[sampleDataKey] = r.data;
                    }
                })
                .catch((error) => {
                    alert(error.message);
                });
        },
    },
});
