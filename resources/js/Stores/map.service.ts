import { defineStore } from 'pinia';
// import axios from 'axios';

export const MapService = defineStore('map', {
    state: () => ({
        // dataset: {} as Dataset,

        mapService: new Map<string, any>(),
    }),
    actions: {
        // payload = authenticated user
        setUser(payload) {
            if (payload.name) {
                this.userName = payload.name;
            }
            if (payload.email) {
                this.userEmail = payload.email;
            }
            if (payload.avatar) {
                this.userAvatar = payload.avatar;
            }
        },

        getMap(id: string) {
            return this.mapService.get(id);
        },

        setMap(id: string, map) {
            this.mapService.set(id, map);
        },

        hasMap(id: string): boolean {
            return this.mapService.has(id);
        },

        deleteMap(id: string): boolean {
            return this.mapService.delete(id);
        },
    },
});
