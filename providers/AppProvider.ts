import type { ApplicationContract } from '@ioc:Adonis/Core/Application';
import type Hash from '@ioc:Adonis/Core/Hash';
// import HttpContextContract from '@ioc:Adonis/Core/HttpContext';
import type Response from '@ioc:Adonis/Core/Response';
import { LaravelHash } from './HashDriver';

export default class AppProvider {
    constructor(protected app: ApplicationContract) {}

    public register() {
        // Register your own bindings
    }

    public async boot() {
        // IoC container is ready
        const hashInstance: typeof Hash = this.app.container.use('Adonis/Core/Hash');
        hashInstance.extend('bcrypt', () => {
            return new LaravelHash();
        });

        const responseInstance: typeof Response = this.app.container.use('Adonis/Core/Response');
        responseInstance.macro('flash', function (key: string, message: any) {
            this.ctx!.session.flash(key, message);
            return this;
        });
        responseInstance.macro('toRoute', function (route: string) {
            this.redirect().toRoute(route);
            return this;
        });
        // this.app.container.singleton('Adonis/Core/Response', () => {
        //     return FlashResponse;
        // });

        // this.app.container.singleton('Adonis/Core/HttpContext', () => {
        // });
    }

    public async ready() {
        // App is ready
    }

    public async shutdown() {
        // Cleanup, since app is going down
    }
}
